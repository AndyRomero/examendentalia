<?php 
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    //incluimos la conxión a la ase de datos
    include 'conexion.php';
    /*
     * Esta función genera el hash con la forma estalecida en el documento 
     * md5(md5($valor).$valor.md5($valor))
     */
    function generaMd5($valorDosLetras){
        $valor =  $valorDosLetras;
        return md5(md5($valor).$valor.md5($valor));
    }

    //Esta variale es por si se quiere uscar en arreglo en lugar de la ase de datos
	$diccionario = array();
    //por codigo ascii generamos de la A a la Z, la primera letra
	for($i=97; $i<=122; $i++) {  
  	  	$primerLetra = chr($i);
        //por codigo ascii generamos de la A a la Z, la segunda letra y concatenamos a la primera
  	  	for ($x=97; $x <= 122 ; $x++) { 
  	  		$segundaLetra = chr($x);
  	  	 	$dosLestras = $primerLetra.$segundaLetra;
            // generamos el hash por medio de la función generaMd5
  	  	 	$md5 = generaMd5($dosLestras);
            //realizamos el insert a la ase de datos
            $sql = "INSERT INTO diccionario (clave, valor)
                    VALUES ('".$dosLestras."','".$md5."');";
            $conn->query($sql);
            echo $sql."<br>";
            //lo agregamos al arreglo diccionario
  	  	 	array_push($diccionario,[$dosLestras => $md5]);
  	  	}

        //generamos el segundo digito de 0 al 9 y los concatenamos a0,etc
  	  	for ($z=0; $z <= 9 ; $z++) { 
  	  		$segundaLetra = $z;
  	  	 	$dosLestras = $primerLetra.$segundaLetra;
  	  	 	$md5 = generaMd5($dosLestras);
  	  	 	$sql = "INSERT INTO diccionario (clave, valor)
                    VALUES ('".$dosLestras."','".$md5."');";
            $conn->query($sql); 
            echo $sql."<br>"; 	 	
  	  	 	array_push($diccionario,[$dosLestras => $md5]);
  	  	} 

        //generamos el primer digito de 0 al 9 y los concatenamos 0a, etc
        for ($z=0; $z <= 9 ; $z++) { 
            $segundaLetra = $z;
            $dosLestras = $segundaLetra.$primerLetra;
            $md5 = generaMd5($dosLestras);
            $sql = "INSERT INTO diccionario (clave, valor)
                    VALUES ('".$dosLestras."','".$md5."');";
            $conn->query($sql); 
            echo $sql."<br>";       
            array_push($diccionario,[$dosLestras => $md5]);
        } 

        //generamos el .a , etc
  	  	$puntoLetra = '.'.$primerLetra;
  	  	$md5 = generaMd5($puntoLetra);
  	  	$sql = "INSERT INTO diccionario (clave, valor)
                    VALUES ('".$puntoLetra."','".$md5."');";
        $conn->query($sql);
        echo $sql."<br>";
  	  	array_push($diccionario,[$puntoLetra => $md5]);

        //generamos a. , etc
  	  	$letraPunto = $primerLetra.'.';
  	  	$md5 = generaMd5($letraPunto);
  	  	$sql = "INSERT INTO diccionario (clave, valor)
                    VALUES ('".$letraPunto."','".$md5."');";
        $conn->query($sql);
        echo $sql."<br>";
  	  	array_push($diccionario,[$letraPunto => $md5]);

        //generamos @a, etc
  	  	$arrobaLetra = '@'.$primerLetra;
  	  	$md5 = generaMd5($arrobaLetra);
  	  	$sql = "INSERT INTO diccionario (clave, valor)
                    VALUES ('".$arrobaLetra."','".$md5."');";
        $conn->query($sql);
        echo $sql."<br>";
  	  	array_push($diccionario,[$arrobaLetra => $md5]);

        //generamos  a@ , etc
  	  	$letraArroba = $primerLetra.'@';
  	  	$md5 = generaMd5($letraArroba);
  	  	$sql = "INSERT INTO diccionario (clave, valor)
                    VALUES ('".$letraArroba."','".$md5."');";
        $conn->query($sql);
        echo $sql."<br>";
  	  	array_push($diccionario,[$letraArroba => $md5]);

    	  
	} 