<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    //Incluimos la conexion a la ase de datos
    include 'conexion.php';

    /*
     * Función para dividir la cadena a decifrar, verificamos que sea multiplo de 32 y generamos un 
     * arreglo en donde guardaremos en un indice cada 32 caracteres
     */
    function divideCadena($cadena = null){
        $numeroCaracteres = 32;
        $arregloCadenaDividida = array();
        if($cadena != null){
            $numeroTotalElementos = strlen($cadena);
            //echo 'numero caracteres:'.$numeroTotalElementos."<br>";
            if(($numeroTotalElementos % $numeroCaracteres) == 0) {
                $divideCadena = $numeroTotalElementos/$numeroCaracteres;
                for ($i = 1; $i <= $divideCadena; $i++) {
                    $inicia = ($i-1) * $numeroCaracteres;
                    $fin = $inicia + $numeroCaracteres;
                    $cadenaDevuelta = '';
                    for ($y = $inicia; $y < $fin ; $y++) {
                        $cadenaDevuelta.=$cadena[$y];
                    }
                    $cadenaDevueltaArreglo[$i-1] = $cadenaDevuelta;
                }
                return $cadenaDevueltaArreglo;
            }else{
                return "NO es una cadena valida";
            }
        }
    }

    //Asignamos el valor de divideCadena a la variale arreglo
    $arreglo = (divideCadena("1ee7b2c95024bf2a3a5b645676875f72af97c725eca5ea2e7ae4a6b18dd9f608aaf180b66f6282f65168c30c1c5e54660aaa8fb0c5b4fa52c0d06ed55d02fcc95e35b5a6a854db008cb8a677dfff030b35306f43e19f6826a5df7937e71d228cb2917b47913c83618903a4e56482283634e4cf70388e9477677aa3013494ba1c3d116007775d60a0d5781d2e35d747b5dece2e0e3d79d272e40c8c66555f5525"));

    $cadenaFinal ="";
    $parteCadenaQuery = "";
    $correo = "";
    $numeroTotalElementosArreglo = count($arreglo);
    for ($i = 0; $i < $numeroTotalElementosArreglo ; $i++) {
        $parteCadena =  $arreglo[$i];
        $parteCadenaQuery.= "'".$arreglo[$i]."',";
        //$cadenaFinal.=array_search($parteCadena, $diccionario);
        $sql = "select clave from diccionario where valor in ('".$arreglo[$i]."');";
        //echo '<br>'.$sql;
        if($resultado = $conn->query($sql)){
            if ($resultado->num_rows > 0) {
                while($obj = $resultado->fetch_object()){ 
                    $correo.=$obj->clave;  
                } 
            }
        }
    }
    echo 'cadena encontrada: '.$correo;
?>